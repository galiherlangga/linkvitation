const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const links = document.querySelectorAll('.nav-links li');
    const navbar = document.querySelector('.navbar');

    burger.addEventListener('click', () => {
        nav.classList.toggle('nav-active');

        links.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = '';
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 1}s`;
            }
        })

        burger.classList.toggle('closeNav');
    });

    window.onscroll = function () {
        if (window.pageYOffset > 100) {
            navbar.style.background = '#293462';
            navbar.style.boxShadow = "0px 0px 1px grey";
        } else {
            navbar.style.background = "transparent";
            navbar.style.boxShadow = "none";
        }
    }

}

const startModal = () => {
    let modal = document.getElementById('modal-start')
    let close = document.getElementById('close2')
    modal.style.display = 'block'

    close.onclick = function () {
        modal.style.display = 'none'
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display ='none'
        }
    }
}

// startModal();
navSlide();
